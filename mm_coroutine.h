//FIXME: need implementations for other architectures. I should probably
//move all the arch-dependent stuff to a separate function. The only thing
//I really need as a way to set the stack pointer to a given variable. 

//FIXME: This code depends on frame pointers. It cannot be compiled with 
//-fomit-frame-pointer. It's definitely possible to fix this, but it's 
//tricky because of the way gcc emits code that accesses local variables 
//(to make a long story short, most of the time gcc accesses a local 
//variable it gets it by indexing rbp, and it's difficult to tell gcc "look
//just put this variable in a register"; I tried using the register keyword
//but it didn't work).

#ifdef MM_IMPLEMENT
	#ifndef MM_COROUTINE_H_IMPLEMENTED
		#define SHOULD_INCLUDE 1
		#define MM_COROUTINE_H_IMPLEMENTED 1
	#else
		#define SHOULD_INCLUDE 0
	#endif
#else
	#ifndef MM_COROUTINE_H
		#define SHOULD_INCLUDE 1
		#define MM_COROUTINE_H 1
	#else 
		#define SHOULD_INCLUDE 0
	#endif
#endif


#if SHOULD_INCLUDE
#undef SHOULD_INCLUDE


#ifdef MM_IMPLEMENT
#undef MM_IMPLEMENT
#include "mm_coroutine.h"
#define MM_IMPLEMENT 1
#endif

#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>
#include <signal.h>

////////////////////////////////
//Structs, typedefs, and enums//
////////////////////////////////

#ifndef MM_IMPLEMENT

typedef enum {
  CRT_STATE_CREATED,
  CRT_STATE_READY,
  CRT_STATE_BLOCKED, //FIXME: currently unused
  CRT_STATE_RUNNING,
  CRT_STATE_DONE,
  CRT_STATE_ERROR
} crt_state;

typedef struct _crt_ctx crt_ctx;

typedef void crt_fn(crt_ctx *ctx, void *arg);

//TODO: I know there is a way to use mmap to make "stack memory" that 
//automatically expands downards. One day I'll figure out how to use it
#define CRT_STACK_SZ 40960
typedef struct _crt_ctx {
    jmp_buf exit_buf;
    jmp_buf resume_buf;
    char stack[CRT_STACK_SZ];
    crt_state state;
    //TODO: something similar to pthread's condition variables?
    crt_fn *fn;
    void *arg;
} crt_ctx;

typedef enum {
  CRT_STATUS_DONE = 1, //This helps us avoid returning zero using longjmp
  CRT_STATUS_IN_PROGRESS,
  CRT_STATUS_ERROR
} crt_status;

#endif

#ifdef MM_IMPLEMENT
static crt_status crt_state_to_status(crt_state s) {
    switch (s) {
    case CRT_STATE_CREATED:
    case CRT_STATE_READY:
    case CRT_STATE_BLOCKED:
    case CRT_STATE_RUNNING:
        return CRT_STATUS_IN_PROGRESS;
    case CRT_STATE_DONE:
        return CRT_STATUS_DONE;
    default:
        return CRT_STATUS_ERROR;
    }
}
#endif

//////////////////////////////////////////
//Functions for managing crt_ctx structs//
//////////////////////////////////////////

crt_ctx *crt_new(crt_fn *fn, void *arg) 
#ifndef MM_IMPLEMENT
;
#else
{
    crt_ctx *ret = malloc(sizeof(crt_ctx));
    if (!ret) {
        fprintf(stderr, "Could not allocate crt_ctx; out of memory\n");
        raise(SIGABRT); 
    }
    
    ret->state = CRT_STATE_CREATED;
    ret->fn = fn;
    ret->arg = arg;

    return ret;
}
#endif

crt_status crt_get_status(crt_ctx *ctx)
#ifndef MM_IMPLEMENT
;
#else
{
    return crt_state_to_status(ctx->state);
}
#endif

//You could just set the struct member directly, but 
//these functions exist to make an explicit API
crt_fn *crt_set_fn(crt_ctx *ctx, crt_fn *fn)
#ifndef MM_IMPLEMENT
;
#else
{
    void *old = ctx->fn;
    ctx->fn = fn;
    return old;
}
#endif

void *crt_set_arg(crt_ctx *ctx, void *arg)
#ifndef MM_IMPLEMENT
;
#else
{
    void *old = ctx->arg;
    ctx->arg = arg;
    return old;
}
#endif

void crt_clear_error(crt_ctx *ctx)
#ifndef MM_IMPLEMENT
;
#else
{
    if (ctx->state == CRT_STATE_ERROR) {
        ctx->state = CRT_STATE_CREATED;
    }
}
#endif

//Gracefully ignores NULL inputs
void crt_del(crt_ctx *ctx) 
#ifndef MM_IMPLEMENT
;
#else
{
    free(ctx);
}
#endif
    

//////////////////////////////////////////////////////
//Functions for calling/yielding/aborting coroutines//
//////////////////////////////////////////////////////

void crt_early_exit(crt_ctx *ctx) 
#ifndef MM_IMPLEMENT
;
#else
{
    int rc = setjmp(ctx->resume_buf);
    if (rc != 0) {
        ctx->state = CRT_STATE_RUNNING;
        return;
    } else {
        ctx->state = CRT_STATE_READY;
        longjmp(ctx->exit_buf, 1); //GCC's __builtin_longjmp forces you to use 1 here...
    }
}
#endif

void crt_error(crt_ctx *ctx)
#ifndef MM_IMPLEMENT
;
#else
{
    while(1) {
        setjmp(ctx->resume_buf);
        ctx->state = CRT_STATE_ERROR;
        longjmp(ctx->exit_buf, 1);
    }
}
#endif

void crt_exit(crt_ctx *ctx)
#ifndef MM_IMPLEMENT
;
#else
{
    ctx->state = CRT_STATE_DONE;
    longjmp(ctx->exit_buf, 1);
}
#endif

crt_status crt_run(crt_ctx *ctx) 
#ifndef MM_IMPLEMENT
;
#else
{
  if (ctx->state == CRT_STATE_DONE) return CRT_STATUS_ERROR;
  int rc = setjmp(ctx->exit_buf);
  if (rc != 0) return crt_state_to_status(ctx->state);

  if (ctx->state == CRT_STATE_READY) {
    //The only other place we need to do something dirty:
    //When _FORTIFY_SOURCE is on, longjmp will enforce that the current
    //stack pointer is less or equal to the targeted stack pointer
    //(remember that stacks grow downward). This is because longjmp 
    //is only intended to be used when the corresponding setjmp was
    //done by an earlier function call on the same stack. To put it
    //differently, any address that is smaller than the current stack
    //pointer is interpreted as being inside a "later" function call.
    //Anyway, we can circumvent this safety check by moving our stack
    //pointer to the top of the coroutine's stack just before calling
    //longjmp (well, not quite the very top, because __longjmp_chk 
    //might make a call to sigaltstack and needs a handful of bytes
    //of space on the stack)
    void *newsp = (void*) ctx->stack + 32; //32 shuld be more than enough
    
    __asm volatile (
#ifdef __x86_64__
      "mov %[newsp], %%rsp \n"
#else
      "mov %[newsp], %%esp \n"
#endif
      : 
      : [newsp] "r" (newsp)
      : 
    );
    longjmp(ctx->resume_buf, 1);
  } else if (ctx->state == CRT_STATE_CREATED || ctx->state == CRT_STATE_DONE) {
    //The only place we need to do anything dirty... forcibly
    //set the stack pointer to the new stack
    //https://brennan.io/2020/05/24/userspace-cooperative-multitasking/
    //This code assumes stacks grow downwards
    void *newsp = (void*) ctx->stack + CRT_STACK_SZ;
#if defined(__MINGW32__) || defined(__MINGW64__) || defined(__CYGWIN__)
    //The calling convention on windows is super weird... a caller has to
    //make space on its own stack for arguments to any called functions,
    //even if it's only using registers to pass arguments. The reason is 
    //because the callee will often immediately save the arguments (passed
    //as registers) into this pre-allocated stack space. This means that
    //when we change to the new stack, we need to artifically make space.
    //What a way to run a railroad...
    newsp -= 32; //32 is extra-safe... technically we only need enough room
                 //for the two arguments to ctx->fn
#endif
    __asm volatile (
#ifdef __x86_64__
      "mov %[newsp], %%rsp \n"
#else
      "mov %[newsp], %%esp \n"
#endif
      : 
      : [newsp] "r" (newsp)
      : 
    );

    //Note: so long as you don't compile with -fomit-frame-pointer,
    //gcc will emit instructions that use rbp to find ctx, which is 
    //why this code is still safe
    ctx->state = CRT_STATE_RUNNING;
    ctx->fn(ctx, ctx->arg);

    ctx->state = CRT_STATE_DONE;
    longjmp(ctx->exit_buf, 1);
  } else {
    fprintf(stderr, "Error: tried to run something invalid\n");
    return CRT_STATUS_ERROR;
  }
}
#endif

void crt_stop(crt_ctx *ctx)
#ifndef MM_IMPLEMENT
;
#else
{
    ctx->state = CRT_STATE_CREATED;
}
#endif

//Make crt_restart a synonym for crt_stop, since they
//would both do the same thing
#ifndef MM_IMPLEMENT
#define crt_restart crt_stop
#endif

#else
#undef SHOULD_INCLUDE
#endif

## Marco's accumulated C libraries

These are C files I noticed myself copying into a lot of different projects. 
Mostly this is inspired by the [stb libraries][1], and I more or less stole the 
idea to use a single "implement.c" files from that project.

[1]: https://github.com/nothings/stb

## How to use

All of these files are `.h` files. Just include them in the usual way, but in
ONE C file, do something like:

```c
#define MM_IMPLEMENT
#include "vector.h"
#include "mm_err.h"
//etc.
```

When the `MM_IMPLEMENT` macro is defined, the many `#ifdef` statements in the 
headers will actually define functions rather than just the prototypes.

## What currently works?

I haven't run any big suite of unit tests (because these are my personal libraries
I just debug them as needed) so I wouldn't use any of this in production code. 
Nonetheless, the following headers seem to be working:

- `mm_err.h`, my personal way of handling errors. There's a big comment at the top
  explaining how to use it
- `list.h`, which is more or less identical to the `list.h` from the Linux kernel.
  It works in all the projects I use it in.
- `vector.h`, which is documented in `/man/vector.3`. This is the one I use the most
- `heap.h` is a set of macros/functions on top of `vector.h`. It seems to be working,
   but it's something I wrote up quickly for one of my projects and it has no real
   documentation
- `map.h` is an implementation of hash maps for C. It works and it's really nice to
  have, but the API needs improvement. There's no documentation on it, sadly.
- `tatham_coroutine.h` is Simon Tatham's implementation of coroutines in C. All I did
  was to package it up as a single-header library, and I added a "reset return" function
  to the simple coroutines. See the header for more info
- `mm_coroutine.h` is my own implementation of stackful coroutines. It works in Linux
  and in Windows (under MinGW) but I haven't tested any other platforms. It is documented
  in `man/mm_coroutine.3`
- `os_common.h` is just a set of wrapper functions and macros to make some common stuff
  uniform across Linux and Windows (such as sockets, gettimeofday, and a few other 
  things)
- `wellons_memalias.h` is a really cool bit of code I found online and put together into
  a header. It allows you to mess around with mapping the same page of memory in 
  multiple places in your process's address space, in both Linux and Windows. 
- `fast_fail.h` and `stringify.h` are just little utility headers used by the other 
   files

## What I'm working on

The `http_parse.h` and `websock.h` headers are part of an ongoing project to write
webapps in C. I assume most people reading this are immediately thinking I'm an 
idiot for wanting to do web dev in C, but hey, it's my life and my code, so quit
judging me already! Currently I want to add a little bit more functionality and 
I desperately want to remove the dependency on OpenSSL (I just need to get around
to providing a SHA-1 implementation, which I may end up needing to do myself).

The `graph.h` library is basically empty because I haven't decided what the API
will look like yet.

I have several projects that use tokenizing, and I've been trying to come with an 
idea for a simple tokenizer API. Sadly, I've never written any tokenizing code that 
I was ultimately happy with. I really like the lpeg library in Lua, though, so maybe
I'll work on a nice C implementation of it.

